package com.sda.finalProject.services;

import com.sda.finalProject.entities.MedicalProceduresRecordEntity;
import com.sda.finalProject.repository.MedicalProceduresRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MedicalProceduresRecordService {

    @Autowired
    private MedicalProceduresRecordRepository medicalProceduresRecordRepository;

    public MedicalProceduresRecordEntity addMedicalProcedureRecord(MedicalProceduresRecordEntity medicalProceduresRecordEntity) {
        return medicalProceduresRecordRepository.save(medicalProceduresRecordEntity);
    }
}
package com.sda.finalProject.services;

import com.sda.finalProject.entities.RoomAppointmentsEntity;
import com.sda.finalProject.repository.RoomAppointmentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoomAppointmentsService {
    @Autowired
    private RoomAppointmentsRepository roomAppointmentsRepository;

    public RoomAppointmentsEntity addAppointment(RoomAppointmentsEntity roomappointment) {
        return roomAppointmentsRepository.save(roomappointment);
    }


    public RoomAppointmentsEntity editRoomAppointment(Integer id, RoomAppointmentsEntity roomAppointment) {
        Optional<RoomAppointmentsEntity> appointmentToEdit = roomAppointmentsRepository.findById(id);
        if (appointmentToEdit.isPresent()) {
            RoomAppointmentsEntity appointmentFounded = appointmentToEdit.get();
            appointmentFounded.setRoomId(roomAppointment.getRoomId());
            appointmentFounded.setUserId(roomAppointment.getUserId());
            return roomAppointmentsRepository.save(appointmentFounded);
        }
        return roomAppointmentsRepository.save(roomAppointment);
    }

    public void deleteUAppointment(Integer id) {
        roomAppointmentsRepository.deleteById(id);
    }
}

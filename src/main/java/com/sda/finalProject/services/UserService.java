package com.sda.finalProject.services;

import com.sda.finalProject.entities.UserEntity;
import com.sda.finalProject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public UserEntity addUser(UserEntity userEntity) {
        return userRepository.save(userEntity);
    }


    public UserEntity editUser(Integer id, UserEntity userEntity) {
        Optional<UserEntity> userToEdit = userRepository.findById(id);
        if (userToEdit.isPresent()) {
            UserEntity userEntityFounded = userToEdit.get();
            userEntityFounded.setAddress(userEntity.getAddress());
            userEntityFounded.setEmail(userEntity.getEmail());
            userEntityFounded.setName(userEntity.getName());
            userEntityFounded.setPassword(userEntity.getPassword());
            userEntityFounded.setPhone(userEntity.getPhone());
            userEntityFounded.setSsn(userEntity.getSsn());
            userEntityFounded.setUsername(userEntity.getUsername());
            userEntityFounded.setAuthorities(userEntity.getAuthorities());
            return userRepository.save(userEntityFounded);
        }
        return userRepository.save(userEntity);
    }

    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }
}

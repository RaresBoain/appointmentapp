package com.sda.finalProject.services;

import com.sda.finalProject.entities.MedicalProcedureEntity;
import com.sda.finalProject.repository.MedicalProcedureRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MedicalProcedureService {
    @Autowired
    private MedicalProcedureRepository medicalProcedureRepository;
    public MedicalProcedureEntity addMedicalProcedure(MedicalProcedureEntity medicalProcedureEntity){
        return medicalProcedureRepository.save(medicalProcedureEntity);
    }
    public MedicalProcedureEntity editMedicalProcedure(Integer medicalProcedureId, MedicalProcedureEntity medicalProcedureEntity){
        Optional<MedicalProcedureEntity> medicalProcedureToEdit = medicalProcedureRepository.findById(medicalProcedureId);
        if (medicalProcedureToEdit.isPresent()) {
            MedicalProcedureEntity medicalProcedureEntityFounded = medicalProcedureToEdit.get();
            medicalProcedureEntityFounded.setMedicalProcedureName(medicalProcedureEntity.getMedicalProcedureName());
            medicalProcedureEntityFounded.setMedicalProcedurePrice(medicalProcedureEntity.getMedicalProcedurePrice());
            return medicalProcedureRepository.save(medicalProcedureEntityFounded);
        }
        return medicalProcedureRepository.save(medicalProcedureEntity);
    }
    public void deleteMedicalProcedure(Integer medicalProcedureId){
        medicalProcedureRepository.deleteById(medicalProcedureId);
    }
}

package com.sda.finalProject.services;

import com.sda.finalProject.entities.RoomEntity;
import com.sda.finalProject.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RoomService {
    @Autowired
    public RoomRepository roomRepository;

    public RoomEntity addRoom(RoomEntity room) {
        return roomRepository.save(room);
    }


    public RoomEntity editRoom(Integer id, RoomEntity room) {
        Optional<RoomEntity> roomToEdit = roomRepository.findById(id);
        if (roomToEdit.isPresent()) {
            RoomEntity roomFounded = roomToEdit.get();
            roomFounded.setNumber(room.getNumber());
            roomFounded.setSchedule(room.getSchedule());
            roomFounded.setUserEntities(room.getUserEntities());
            return roomRepository.save(roomFounded);
        }
        return roomRepository.save(room);
    }

    public void deleteRoom(Integer id) {
        roomRepository.deleteById(id);
    }
}

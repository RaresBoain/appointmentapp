package com.sda.finalProject.entities;

import javax.persistence.*;

@Entity
@Table(name = "room_appointments_entity")
public class RoomAppointmentsEntity {

    //id, user ca relatie, room ca relatie
    //inceput programare-sfarsit programare
    //relatie manytomany

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer roomAppointmentId;

    @Column(name = "userId")
    private Integer userId;
    @Column(name = "roomId")
    private Integer roomId;

    public RoomAppointmentsEntity() {
    }

    public Integer getRoomAppointmentId() {
        return roomAppointmentId;
    }

    public void setRoomAppointmentId(Integer roomAppointmentId) {
        this.roomAppointmentId = roomAppointmentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }
}

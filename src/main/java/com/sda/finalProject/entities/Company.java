package com.sda.finalProject.entities;

public class Company {

    private String name = "Medical Facilities";

    private String address = "30 Greenwood Crescent, York, UK ";

    private String phone = "00447404027779";

    private String email = "medical@medical.com";

    private String bankAccount = "112233445566";

    public Company() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }
}

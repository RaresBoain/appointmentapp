package com.sda.finalProject.entities;

import com.sda.finalProject.entities.UserEntity;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "medicalProcedures")
public class MedicalProcedureEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicalProcedureId;
    @Column
    public String medicalProcedureName;
    public Integer medicalProcedurePrice;
    @ManyToMany
    @JoinTable(
            name = "medical_procedures_record",
            joinColumns = @JoinColumn(name = "medicalProcedureId"),
            inverseJoinColumns = @JoinColumn(name = "userId")
    )
    private List<UserEntity> userEntities;

    public MedicalProcedureEntity(String medicalProcedureName, Integer medicalProcedurePrice, LocalDateTime medicalProcedureDateTime) {
        this.medicalProcedureName = medicalProcedureName;
        this.medicalProcedurePrice = medicalProcedurePrice;
    }

    public MedicalProcedureEntity() {
    }

    ;

    public String getMedicalProcedureName() {
        return medicalProcedureName;
    }

    public void setMedicalProcedureName(String medicalProcedureName) {
        this.medicalProcedureName = medicalProcedureName;
    }

    public Integer getMedicalProcedurePrice() {
        return medicalProcedurePrice;
    }

    public void setMedicalProcedurePrice(Integer medicalProcedurePrice) {
        this.medicalProcedurePrice = medicalProcedurePrice;
    }

    public Integer getMedicalProcedureId() {
        return medicalProcedureId;
    }

    public void setMedicalProcedureId(Integer medicalProcedureId) {
        this.medicalProcedureId = medicalProcedureId;
    }

    public List<UserEntity> getUserEntities() {
        return userEntities;
    }

    public void setUserEntities(List<UserEntity> userEntities) {
        this.userEntities = userEntities;
    }
}
















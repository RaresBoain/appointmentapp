package com.sda.finalProject.entities;

import javax.persistence.*;

@Entity
@Table(name = "medical_procedures_record")
public class MedicalProceduresRecordEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer medicalProceduresRecordId;
    @Column(name = "userId")
    private Integer userId;
    @Column(name = "medicalProcedureId")
    private Integer medicalProcedureId;

    public MedicalProceduresRecordEntity() {
    }

    public void setMedicalProceduresRecordId(Integer medicalProceduresRecordId) {
        this.medicalProceduresRecordId = medicalProceduresRecordId;
    }

    public Integer getMedicalProceduresRecordId() {
        return medicalProceduresRecordId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMedicalProcedureId() {
        return medicalProcedureId;
    }

    public void setMedicalProcedureId(Integer medicalProcedureId) {
        this.medicalProcedureId = medicalProcedureId;
    }
}

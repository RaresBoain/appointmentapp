package com.sda.finalProject.repository;

import com.sda.finalProject.entities.MedicalProcedureEntity;
import com.sda.finalProject.entities.MedicalProceduresRecordEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalProceduresRecordRepository extends JpaRepository<MedicalProceduresRecordEntity,Integer> {
}

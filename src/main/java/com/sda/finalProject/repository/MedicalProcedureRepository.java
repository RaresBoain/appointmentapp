package com.sda.finalProject.repository;

import com.sda.finalProject.entities.MedicalProcedureEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MedicalProcedureRepository extends JpaRepository<MedicalProcedureEntity,Integer> {
}

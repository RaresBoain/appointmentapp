package com.sda.finalProject.repository;

import com.sda.finalProject.entities.RoomAppointmentsEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoomAppointmentsRepository extends JpaRepository<RoomAppointmentsEntity, Integer> {
}

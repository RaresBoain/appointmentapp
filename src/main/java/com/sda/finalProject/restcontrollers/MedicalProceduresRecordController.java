package com.sda.finalProject.restcontrollers;

import com.sda.finalProject.entities.MedicalProcedureEntity;
import com.sda.finalProject.entities.MedicalProceduresRecordEntity;
import com.sda.finalProject.entities.RoomAppointmentsEntity;
import com.sda.finalProject.services.MedicalProceduresRecordService;
import com.sda.finalProject.services.RoomAppointmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MedicalProceduresRecordController {

    @Autowired
    public MedicalProceduresRecordService medicalProcedureRecordService;

    @PostMapping("/addMedicalProcedureRecord")
    public MedicalProceduresRecordEntity addMedicalProcedureRecord(@RequestBody MedicalProceduresRecordEntity medicalProceduresRecordEntity) {
        return medicalProcedureRecordService.addMedicalProcedureRecord(medicalProceduresRecordEntity);
    }

}

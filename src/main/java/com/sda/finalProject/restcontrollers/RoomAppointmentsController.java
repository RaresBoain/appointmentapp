package com.sda.finalProject.restcontrollers;

import com.sda.finalProject.entities.RoomAppointmentsEntity;
import com.sda.finalProject.services.RoomAppointmentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoomAppointmentsController {
    @Autowired
    public RoomAppointmentsService roomAppointmentsService;

    @PostMapping("/addRoomAppointment")
    public RoomAppointmentsEntity addAppointment(@RequestBody RoomAppointmentsEntity appointment) {
        return roomAppointmentsService.addAppointment(appointment);
    }

    @PutMapping("/editRoomAppointment/{id}")
    public RoomAppointmentsEntity editAppointment(@PathVariable Integer id, @RequestBody RoomAppointmentsEntity appointment) {
        return roomAppointmentsService.editRoomAppointment(id, appointment);
    }

    @DeleteMapping("/deleteRoomAppointment/{id}")
    public void deleteAppointment(@PathVariable Integer id) {
        roomAppointmentsService.deleteUAppointment(id);
    }

}

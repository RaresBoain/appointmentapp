package com.sda.finalProject.restcontrollers;

import com.sda.finalProject.entities.RoomEntity;
import com.sda.finalProject.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoomController {
    @Autowired
    public RoomService roomService;

    @PostMapping("/addRoom")
    public RoomEntity addRoom(@RequestBody RoomEntity room) {
        return roomService.addRoom(room);
    }

    @PutMapping("/editRoom/{id}")
    public RoomEntity editRoom(@PathVariable Integer id, @RequestBody RoomEntity room) {
        return roomService.editRoom(id, room);
    }

    @DeleteMapping("/deleteRoom/{id}")
    public void deleteRoom(@PathVariable Integer id) {
        roomService.deleteRoom(id);
    }
}

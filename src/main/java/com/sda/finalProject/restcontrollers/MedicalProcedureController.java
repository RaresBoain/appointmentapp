package com.sda.finalProject.restcontrollers;

import com.sda.finalProject.entities.MedicalProcedureEntity;
import com.sda.finalProject.services.MedicalProcedureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class MedicalProcedureController {
    @Autowired
    MedicalProcedureService medicalProcedureService;
    @PostMapping("/addMedicalProcedure")
    public MedicalProcedureEntity addMedicalProcedure(@RequestBody MedicalProcedureEntity medicalProcedureEntity){
        return medicalProcedureService.addMedicalProcedure(medicalProcedureEntity);
    }
    @PutMapping("/editMedicalProcedure/{medicalProcedureId}")
    public MedicalProcedureEntity editMedicalProcedure(@PathVariable Integer medicalProcedureId, @RequestBody MedicalProcedureEntity medicalProcedureEntity){
        return medicalProcedureService.editMedicalProcedure(medicalProcedureId, medicalProcedureEntity);
    }
    @DeleteMapping("/deleteMedicalProcedure/{medicalProcedureId}")
    public void deleteMedicalProcedure(@PathVariable Integer medicalProcedureId){
        medicalProcedureService.deleteMedicalProcedure(medicalProcedureId);
    }
}

package com.sda.finalProject.restcontrollers;

import com.sda.finalProject.entities.UserEntity;
import com.sda.finalProject.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {

    @Autowired
    public UserService userService;

    @PostMapping("/addUser")
    public UserEntity addUser(@RequestBody UserEntity userEntity) {
        return userService.addUser(userEntity);
    }

    @PutMapping("/editUser/{userId}")
    public UserEntity editUser(@PathVariable Integer userId, @RequestBody UserEntity userEntity) {
        return userService.editUser(userId, userEntity);
    }

    @DeleteMapping("/deleteUser/{userId}")
    public void deleteUser(@PathVariable Integer userId) {
        userService.deleteUser(userId);
    }
}
